import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'noImage'
})
export class NoImagePipe implements PipeTransform {

  transform(urlparam: any[]): string {
    let img_default='assets/img/noimage.png';
    if(!urlparam){
      return img_default;
    }
    return (urlparam.length>0) ? urlparam[1].url: img_default;
  }

}
