import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {log} from "util";


@Injectable()
export class SpotifyService {
  artistas: any[] = [];
  urlBusqueda: string = "https://api.spotify.com/v1/search";
  urlArtista: string = "https://api.spotify.com/v1/artists/";

  constructor(private http: Http) {
  }


  getArtistas(termino: string) {
    let query = `?q=${termino}&type=artist`;
    let url = this.urlBusqueda + query;
    let headers=this.getHeader();
    return this.http.get(url, {headers}).map(
      res => {
        //console.log(res.json());
        this.artistas = res.json().artists.items;
        //console.log(this.artistas);
        return this.artistas;
      }
    );

  }

  getOneArtistById(id: string) {
    const query = this.urlArtista + id;
    console.log("ERRorororor aaqui!!");
    let headers =this.getHeader();

    return this.http.get(query, {headers}).map(
      res => {
        //console.log(res.json());
        //this.artistas=res.json().artists.items;
        //console.log(this.artistas);
        return res.json();
      }
    );
  }

  getTopTracksByIdArtist(id: string) {
    const query = this.urlArtista +`${id}/top-tracks?country=MX`;

    let headers=this.getHeader();
    return this.http.get(query, {headers}).map(
      res => {
        console.log(res.json());
        //this.artistas=res.json().artists.items;
        //console.log(this.artistas);
        return res.json().tracks;
      }
    );
  }

  getHeader(){
    let headers = new Headers();
    headers.append('authorization', 'Bearer BQDXpMJFzj9TS30FfpR8kNZyGQ4sH6l3cRAwG9d5TXYKupP1yj0b4vnik1HBXt6A0scxhy8sp9aA0CT59J84kKl0HWmvgqFtejlwJv_0FO_2ryApyX0uG38Bl6Vh0E_oPbzNOEnl7a8yYlqn');
    return headers;
  }


}
