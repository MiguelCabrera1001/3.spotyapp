import { Component, OnInit } from '@angular/core';
import {SpotifyService} from '../../services/spotify.service';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html'
})
export class SearchComponent implements OnInit {
  termino:string='';
  search_artistas:any[]=[];
  constructor(private _spotifyService:SpotifyService) { }

  ngOnInit() {

  }
  buscarArtista(){
    if(this.termino){
      this._spotifyService.getArtistas(this.termino)
      .subscribe(data=>{
        //console.log(data);
        this.search_artistas=data;

      });
    }else{
      this.search_artistas=[];
    }
  }

}
