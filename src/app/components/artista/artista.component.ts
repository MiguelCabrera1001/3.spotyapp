import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SpotifyService} from "../../services/spotify.service";

@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html'
})
export class ArtistaComponent implements OnInit {

  artista:any;
  topTracks:any;
  constructor(private router: ActivatedRoute,private _service:SpotifyService) {


  }

  ngOnInit() {
    this.router.params.map(parametros => parametros['id']
    ).subscribe(id => {
      this._service.getOneArtistById(id).subscribe((data)=>this.artista=data);
      this._service.getTopTracksByIdArtist(id).subscribe((data)=>{
        this.topTracks=data;
        console.log(data);
      });


    })
  }

}
